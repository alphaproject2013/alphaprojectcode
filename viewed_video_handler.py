import webapp2
from google.appengine.ext import db

import constants
import cookie_handler
import db_entities

class ViewedVideo(cookie_handler.ExtendedRequestHandler):
  def get(self):
    video_id = self.request.get("videoid")
    user_id = self.get_else_set_cookie()
    user_entered_search_term = self.request.get("search_term")
    # Create an entity of type UserViewedVideos with parent being of hierarchy - UserId -> SearchTerm
    user_search_terms_key = db.Key.from_path('UserList', user_id, 'UserSearchTerms', user_entered_search_term)
    user_viewed_video = db_entities.UserViewedVideos(parent=user_search_terms_key, video_id=video_id, video_viewed=True)
    user_viewed_video.put()
  
    self.response.headers['Content-Type'] = 'image/gif'  
    self.response.out.write(constants.DOMAIN_HOSTNAME + "/images/transparent-pixel.gif")
