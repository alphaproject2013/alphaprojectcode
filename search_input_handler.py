import urllib
import webapp2
import gdata.youtube.service

from google.appengine.ext import db

import constants
import cookie_handler
import db_entities

class SearchInput(cookie_handler.ExtendedRequestHandler):
  def post(self):
    # TODO: perhaps check if it's a valid cookie in this handler as well, since a user
    # could simply get to the play_video handler.
    user_id = self.get_else_set_cookie()
    user_entered_search_term = self.request.get("searchInput")
    search_term = urllib.unquote(user_entered_search_term.lower())

    query = gdata.youtube.service.YouTubeVideoQuery()
    query.orderby = 'relevance'
    query.racy = 'include'
    query.vq = search_term
    query.max_results = constants.YT_SEARCH_QUERY_MAX_RESULTS
    query.start_index = 1

    user_entry = db.get(db.Key.from_path('UserList', user_id))
    if user_entry is None:
      user_entry = db_entities.UserList(key_name=user_id)
      user_entry.put()

    user_search_term_entry = db.get(db.Key.from_path('UserSearchTerms', user_id))
    if user_search_term_entry is None:
      user_search_term_entry = db_entities.UserSearchTerms(parent=user_entry, key_name=user_entered_search_term, search_term=user_entered_search_term)
      user_search_term_entry.put()

    total_search_pages_requested = 1
    feeds = []
    for i in range(total_search_pages_requested):
      feeds.append(constants.client.YouTubeQuery(query))
      query.start_index = int(query.start_index) + int(query.max_results)

    search_term_videos_entity = db.get(db_entities.search_term_videos_key(user_entered_search_term))
    if search_term_videos_entity is None:       
      search_term_videos_entity = db_entities.SearchTermVideos(key_name=user_entered_search_term,search_term=user_entered_search_term)
      search_term_videos_entity.put()
      for feed in feeds:
        for entry in feed.entry:
          id_substr_text = "gdata.youtube.com/feeds/videos/"
          video_id = ""
          id_string = entry.id.text
          id_start_pos = id_string.find(id_substr_text) + len(id_substr_text)
          video_id = id_string[id_start_pos:]
          video_entry = db_entities.SearchTermVideos(parent=search_term_videos_entity)
          video_entry.video_id = video_id
          video_entry.put()
    self.redirect("/playvideo?searchterm=%s" % user_entered_search_term)
