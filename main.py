import uuid
import webapp2
import time

from google.appengine.api import urlfetch 
from google.appengine.api import users
from google.appengine.ext import db
import cookie_handler
import db_entities
import search_input_handler
import welcome_page_handler
import play_video_handler
import viewed_video_handler
import shared_video_handler
import join_limited_beta_handler
      
class MainPage(cookie_handler.ExtendedRequestHandler):
  def get(self):
    user_id = self.get_else_set_cookie()
    # To start out with, we allow only upto 5 users enforced by checking # of user keys in VideoList.
    if user_id is None:
      self.response.out.write("Sorry, we are in limited beta right now. Please check back later!")
      return

    userlist_key = db.Key.from_path('UserList', user_id)
    q = db_entities.UserSearchTerms.all()
    q.ancestor(userlist_key)
    q.order('-date')
    last_user_search_term = q.get()
    if last_user_search_term is not None:
      if last_user_search_term.search_term != "":
        self.redirect("/playvideo?searchterm=%s" % last_user_search_term.search_term)
        return
    self.redirect("/welcomesearch")


app = webapp2.WSGIApplication([("/", MainPage),
			                   ("/welcomesearch", welcome_page_handler.WelcomeSearchPage),
			                   ("/searchinput", search_input_handler.SearchInput),
                               ("/playvideo", play_video_handler.PlayVideo),
                               ("/viewedvideo", viewed_video_handler.ViewedVideo),
                               ("/sharedvideo", shared_video_handler.SharedVideo),
                               ("/joinlimitedbeta1531", join_limited_beta_handler.JoinLimitedBeta)],
                               debug=True)
