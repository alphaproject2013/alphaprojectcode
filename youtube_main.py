import webapp2
import gdata.youtube.service
import gdata.alt.appengine
import time

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.api import oauth

class VideoList(db.Model):
  author = db.StringProperty()
  video_id = db.StringProperty()
  video_completed = db.BooleanProperty()


def videolist_key(username=None):
  return db.Key.from_path("VideoList", username or "default_user1")


class PlayVideo(webapp2.RequestHandler):
  def get(self):
    user = self.request.get("user")
    print "username is %s" % (user)
    users = [user]
    videos_list = db.GqlQuery("SELECT * "
                              "FROM VideoList "
                              "WHERE author IN :1",
                              users)
    
    video_id = ""
    for video in videos_list:
      print "video_id is %s" % (video.video_id)
      print "video_completed is %s" % (video.video_completed)
      if video.video_completed is False:
        video_id = video.video_id
        video.video_completed = True
        db.put(video)
        break

    self.response.out.write("""
      <div id='player'></div>
      <script src='http://www.youtube.com/player_api'></script>
      <script>
      // create youtube player
      var player;
      function onYouTubePlayerAPIReady() {
	player= new YT.Player('player', {
	height: '390',
	width: '640',
	videoId: '%s',
	events: {
	  'onReady': onPlayerReady,
	  'onStateChange': onPlayerStateChange
	}
	});
      }
      //autoplay video
      function onPlayerReady(event) {
        event.target.playVideo();
      }
      // when video gets over
      function onPlayerStateChange(event) {
        if (event.data === 0) {
	  //alert('yo yo yo');
	  window.location.href = 'http://localhost:8080/playvideo?user=default_user1';
	  //onPlayerReady(event)
        }
      }
      </script>""" % (video_id)) 



class MainPage(webapp2.RequestHandler):
  def get(self):
    user = users.get_current_user()
    if user is None:
      self.redirect(users.create_login_url(self.request.uri))

    # Get the YoutubeService client.
    client = gdata.youtube.service.YouTubeService()
    gdata.alt.appengine.run_on_appengine(client)

    query = gdata.youtube.service.YouTubeVideoQuery()
    query.orderby = 'viewCount'
    query.racy = 'include'
    list_of_search_terms = ['Vadivelu']
    for search_term in list_of_search_terms:
      new_term = search_term.lower()
      query.categories.append('/%s' % new_term)
    feed = client.YouTubeQuery(query)

    id_substr_text = "gdata.youtube.com/feeds/videos/"
    for entry in feed.entry:
      video_id = ""
      id_string = entry.id.text
      id_start_pos = id_string.find(id_substr_text) + len(id_substr_text)
      video_id = id_string[id_start_pos:]
      video_list = VideoList(parent=videolist_key("default_user1"))
      video_list.author = "default_user1"
      video_list.video_id = video_id
      video_list.video_completed = False
      video_list.put()
      time.sleep(1)
    self.redirect("http://localhost:8080/playvideo?user=default_user1")


def getClient():
  client = gdata.youtube.service.YouTubeService()
  consumer_key = 'anonymous'
  consumer_secret = 'anonymous'
  client.SetOAuthInputParameters(
      gdata.auth.OAuthSignatureMethod.HMAC_SHA1,
      consumer_key=CONSUMER_KEY,
      consumer_secret=CONSUMER_SECRET)
  gdata.alt.appengine.run_on_appengine(client)
  return client

