import webapp2
import jinja2

import constants
import cookie_handler

class WelcomeSearchPage(cookie_handler.ExtendedRequestHandler):
  def get(self):
    # TODO: perhaps check if it's a valid cookie in this handler as well, since a user
    # could simply get to the welcome_page_handler
    user_id = self.get_else_set_cookie()
    template = constants.jinja_environment.get_template('welcomesearch.html')
    self.response.out.write(template.render())
