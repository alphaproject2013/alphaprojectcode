import webapp2
from google.appengine.ext import db

import constants
import cookie_handler
import db_entities


class PlayVideo(cookie_handler.ExtendedRequestHandler):
  def get(self):
    user_id = self.get_else_set_cookie()
    user_entered_search_term = self.request.get("searchterm")
    userlist_key = db.get(db.Key.from_path('UserList', user_id))
    q = db_entities.UserSearchTerms.all()
    q.ancestor(userlist_key)
    search_terms = []
    for search_term_entry in q.run():
      search_terms.append(search_term_entry.search_term)

    # First create a list of all video-ids the user has seen.
    user_viewed_videos_query = db_entities.UserViewedVideos.all()
    user_search_terms_key = db.get(db.Key.from_path('UserList', user_id, 'UserSearchTerms', user_entered_search_term))
    user_viewed_videos_query.ancestor(user_search_terms_key)
    user_viewed_videos = []
    for user_viewed_video in user_viewed_videos_query.run():
      if user_viewed_video.video_viewed is True:
        user_viewed_videos.append(user_viewed_video.video_id)
    
    # Now get all videos for this search-term populated in a list minus the videos the user has already viewed
    # for this search-term
    search_term_videos_entity = db.get(db_entities.search_term_videos_key(user_entered_search_term))
    search_term_videos_query = db.query_descendants(search_term_videos_entity)
    user_videos_playlist = []
    for search_term_video in search_term_videos_query.run():
      if search_term_video.video_id not in user_viewed_videos:
        user_videos_playlist.append(search_term_video.video_id)

    if len(user_videos_playlist) <= 0:
      self.redirect("/welcomesearch")
    template_values = {
        'video_ids': user_videos_playlist[0:25],
        'search_terms': search_terms[0:10],
        'current_search_term': user_entered_search_term,
    }
    template = constants.jinja_environment.get_template('playvideo.html')
    self.response.out.write(template.render(template_values))
