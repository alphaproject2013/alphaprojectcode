from google.appengine.ext import db

class UserList(db.Model):
  author = db.StringProperty()

class UserSearchTerms(db.Model):
  date = db.DateTimeProperty(auto_now_add=True)
  search_term = db.StringProperty()  

class UserViewedVideos(db.Model):
  date = db.DateTimeProperty(auto_now_add=True)
  video_id = db.StringProperty()
  video_viewed = db.BooleanProperty()

class SearchTermVideos(db.Model):
  date = db.DateTimeProperty(auto_now_add=True)
  video_id = db.StringProperty()
  search_term = db.StringProperty()
  #view_count = db.IntegerProperty()

class VideoList(db.Model):
  date = db.DateTimeProperty(auto_now_add=True)
  author = db.StringProperty()
  search_term = db.StringProperty()
  video_id = db.StringProperty()
  video_viewed = db.BooleanProperty()


def videolist_key(user=None):
  return db.Key.from_path("VideoList", user)

def search_term_videos_key(search_term=None):
  return db.Key.from_path("SearchTermVideos", search_term)
