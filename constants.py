import jinja2
import os
import gdata.alt.appengine
import gdata.youtube.service

MAX_USERS_ALLOWED = 100000
LIVE_TRAFFIC_PCT = 1
USE_LOCALLY = False
DOMAIN_HOSTNAME = "http://www.nostop.tv"

if USE_LOCALLY is True:
  DOMAIN_HOSTNAME = 'http://localhost:8080'

YT_SEARCH_QUERY_MAX_RESULTS = 50

jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))
client = gdata.youtube.service.YouTubeService()
gdata.alt.appengine.run_on_appengine(client)
